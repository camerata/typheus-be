from flask import jsonify, abort, make_response
from flask_login import login_required

from Typheus import app


@app.errorhandler(404)
def not_found(_):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route("/")
def index():
    return jsonify({"hello": "world"})


@app.route("/navbar")
@login_required
def navbar():
    return jsonify({}), 200

