from peewee import *

from Typheus.settings import DATABASE_NAME, DATABASE_CONFIG


postgres_db = PostgresqlDatabase(
    DATABASE_NAME,
    **DATABASE_CONFIG
)


class PostgresModel(Model):
    class Meta:
        database = postgres_db


class User(PostgresModel):

    username = CharField()

    @property
    def is_authenticated(self):
        pass

    @property
    def is_active(self):
        pass

    @property
    def is_anonymous(self):
        pass

    def get_id(self):
        pass
