import os
import importlib

from flask import Flask
from flask_login import LoginManager

from Typheus.models.user import User


app = Flask(__name__)
app.config.from_object(__name__)


app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'typheus.db'),
    SECRET_KEY='',
    USERNAME='',
    PASSWORD=''
))

app.config.from_envvar('TYPHEUS_SETTINGS', silent=True)

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.get(User.username == user_id)


importlib.import_module("Typheus.views")
