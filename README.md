# Typheus

Backend api server that serves Typheus app

## Getting Started
### Installing

First step to install this projects will be cloning this repo

```bash
$ git clone git@bitbucket.org:pollux_cast0r/typheus-be.git
$ cd typheus-be
```

Project is build around **Flask**. All needed requirements are stored in *requirements.txt* file

```bash
$ pip install requirements.txt
```

The configure your database settings

```bash
$ cp Typheus/settings/example/database.py Typheus/settings/database.py
$ vi Typheus/settings/database.py
```

Then, to run the project
```bash
$ FLASK_APP=Typheus flask run
```

## Running the tests


## Deployment


## Built With

* [Flask](http://flask.pocoo.org/) - The web framework used


## Authors

* [Alexander Svito](https://bitbucket.org/pollux_cast0r)
* [Turlai Vlad](https://bitbucket.org/turlaivlad)
